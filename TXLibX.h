//{===========================================================================================================
/**
 * @file TXLibX.h
 *
 * Библиотека тупого художника для *nix систем
 * 
 * $Version: 1.5[alpha] $
 * $Revision: 28 $
 * $Copyright: (C) Andrew Bezzubtsev 2015-2016 $
 * $Date: Sat. 28 May, 2016 $
 * $Author: Andrew S. Bezzubtsev(<a href=mailto:andrew@bezzubtsev.ru>E-mail</a>) $
 * $Bugtrackers: <a href=https://github.com/Andrew-Bezzubtsev/TXLibX/issues/new>GitHub</a> $
 * 
 * Библиотека Тупого Художника (The Dumb Artist Library, TX Library, TXLib, TXLibX) - это компактная библиотека 
 * двумерной графики для Win32 на С++, намеренно выдержанная в стиле сугубого минимализма. 
 * Это небольшая "песочница" для начинающих, реализованная с целью помочь им в изучении простейших 
 * принципов программирования. Методическое учебное пособие для обучения основ программирования на С++. 
 * Позволяет писать прямолинейный графический код, не заботясь о событийной модели приложений в Win32 
 * / X Window System. Документация на русском языке.
 * 
 * Философия TX Library - облегчить первые шаги в программировании и подтолкнуть к творчеству и 
 * самостоятельности. Исходный текст библиотеки может использоваться для иллюстрации элементарных приемов 
 * работы с окнами Windows/X11, механизмом сообщений Win32/X11, графикой, работой с меню, 
 * растровыми образами, простейшей многопоточностью.
 * 
 * @warning Использование TXLibX требует согласования этого с автором библиотеки
 */
//============================================================================================================
/**
 * @defgroup Mouse        Поддержка мыши
 * @defgroup Keyboard     Поддержка клавиатуры
 * @defgroup Drawing      Рисование
 * @defgroup Miscellanous Разное
 * @defgroup Macros       Макросы
 */
//}===========================================================================================================

#ifndef TXLIBX_H
#define TXLIBX_H

#if !defined(__cplusplus)
#error ------------------------------------------------------------
#error
#error TXLibX build message:
#error you are trying to build TXLibX application
#error using non-C++ compiler.
#error Try using 'c++ {here your compilation flags go}'
#error 
#error ------------------------------------------------------------
#endif

#if !defined(__linux__) && !defined(__bsdi__) && !defined(macintosh) && !defined(Macintosh) && \
    !defined(__MACH__) && !defined(__CYGWIN__)
#error ------------------------------------------------------------
#error
#error TXLibX build message:
#error you are trying to build TXLibX application
#error using non-supported OS. Use Cygwin environment
#error or some Linux, BSD or Macintosh!
#error
#error ------------------------------------------------------------ 
#endif

#include <X11/X.h>
#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/extensions/Xdbe.h>
#include <X11/keysymdef.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <errno.h>
#include <stdint.h>

#include <string>
#include <sstream>
#include <algorithm>
#include <map>

#include <pthread.h>
#include <unistd.h>
#include <signal.h>

#if defined(Macintosh) || defined(macintosh) || (defined(__MACH__) && defined(__APPLE__))
#include <libproc.h>
#endif

#ifndef TX_DEBUG_MACROS
#define TX_DEBUG_MACROS

#if !defined(assert) && !defined(TX_APP_RELEASE_VERSION)

//{===========================================================================================================
/**
 * @ingroup Macros
 * @brief Отладочный макрос assert
 *
 * @arg expr выражение для проверки
 * 
 * Данный макрос проверяет на истинность выражение expr...
 * В случае неверности, об этом сообщается в консоль и программа аварийно завершается.
 * Данный макрос не cработает, если определить TX_APP_RELEASE_VERSION.
 * 
 * @code
 * // Assert will work
 * void func_graph()
 * {
 *      ...
 *      assert(vertices);
 *      ...
 * }
 * 
 * //=====================
 * 
 * // Assert won't  work
 * #define TX_APP_RELEASE_VERSION 1
 *
 * void func_graph()
 * {
 *     ...
 *     assert(vertices);
 *     ...
 * }
 * @endcode
 *
 * @see TX_ERROR()
 */
//}===========================================================================================================

#define assert(expr) {if (!expr) {TX_ERROR("Error: '%s' is invalid, assertion failed!\n", #expr);}}

#define $UPDATE_LOGGER_CONFIG \
{\
	::__TXLibX_SYS__::logger::func = __PRETTY_FUNCTION__;\
	::__TXLibX_SYS__::logger::file = __FILE__;\
	::__TXLibX_SYS__::logger::line = __LINE__;\
}

#define $TX_ENTRY \
	$UPDATE_LOGGER_CONFIG\
	::__TXLibX_SYS__::logger::tx_logger __default_tx_logger;

#elif defined(TX_APP_RELEASE_VERSION)
#define assert(expr) ((void)0);
#define $UPDATE_LOGGER_CONFIG
#define $TX_ENTRY
#endif
#endif
#define TX_FUNC (::__TXLibX_SYS__::logger::func)
#define TX_FILE (::__TXLibX_SYS__::logger::file)
#define TX_LINE (::__TXLibX_SYS__::logger::line)

#ifndef TX_SYSMACROS
#define TX_SYSMACROS

#define __TX_ERROR__(text, ...) {fprintf(stderr, text, ##__VA_ARGS__); exit(EXIT_FAILURE);}

//{===========================================================================================================
/**
 * @ingroup Macros
 * @brief Отладочный макрос TX_ERROR
 *
 * @arg fmt формат для вывода
 * @arg ... аргументы для вывода
 *
 * Данный макрос сообщает о произошедшей ошибке и аварийно завершает выполнение программы.
 * Данный макрос сработает даже если определить TX_APP_RELEASE_VERSION
 * 
 * @code
 * void rendering()
 * {
 *     ...
 *     if (!ok)
 *         TX_ERROR("Some error occured: ...", ...)
 *     ...
 * }
 * @endcode
 *
 * @see assert()
 */
//}===========================================================================================================

#define TX_ERROR(fmt, ...) __TX_ERROR__("Fatal TXLib error: \n" fmt "\n", ##__VA_ARGS__)

#define TX_FATAL_NONFAIL if (txOK()) TX_ERROR("TXLib requests itself not running by %s function\n"\
                                              "in file %s on line %d.", __PRETTY_FUNCTION__,\
                                              __FILE__, __LINE__)

#define TX_FATAL_FAIL if (!txOK()) TX_ERROR("TXLib requests itself running by %s function\n"\
                                            "in file %s on line %d.", __PRETTY_FUNCTION__,\
                                            __FILE__, __LINE__)

#define TX_TIMEOUT (3 * CLOCKS_PER_SEC)

#define $txDraw(act) (txUnlock((txLock(), (act))))

#endif

#ifndef TX_METRICS
#define TX_METRICS

//{===========================================================================================================
/**
 * @ingroup Macros
 * @brief Маленький размер буфера(строки)
 *
 * Данный макрос, скорее всего будет использован при выделении памяти под строку или буфер ввода/вывода...
 *
 * @code
 * void formulae_reader() 
 * {
 *     ...
 *     char str[TX_BUFSIZE] = "";
 *     fgets(str, sizeof(str), fd);
 *     ...
 * }
 * @endcode
 *
 * @see TX_BIG_BUFSIZE
 */
//}===========================================================================================================

#define TX_BUFSIZE 2048

//{===========================================================================================================
/**
 * @ingroup Macros
 * @brief Большой размер буфера(строки)
 *
 * Данный макрос, скорее всего будет использован при выделении памяти под строку или буфер ввода/вывода...
 *
 * @code
 * void formulae_reader() 
 * {
 *     ...
 *     char str[TX_BIG_BUFSIZE] = "";
 *     fgets(str, sizeof(str), fd);
 *     ...
 * }
 * @endcode
 *
 * @see TX_BUFSIZE
 */
//}===========================================================================================================

#define TX_BIG_BUFSIZE 4096

#endif

#ifndef TX_MISC
#define TX_MISC
#define ZERO(t) (t{})
#endif /* TX_MISC */

//{===========================================================================================================
/**
 * @ingroup Miscellanous
 * @brief Тип данных для хранения цвета
 *
 * Цвет состоит из трех компонент:
 * <ol>
 *   <li>Красный</li>
 *   <li>Зеленый</li>
 *   <li>Синий</li>
 * </ol>
 *
 * Каждая компонента занимает по байту(8 бит).
 * Каждая компонента - целое число от 0 до 255(включительно).
 *
 * @code
 * void rendering()
 * {
 *     txColor custom = txRGB(255, 255, 61);
 *     ...
 * }
 * @endcode
 *
 * @see txRGB()
 */
//}===========================================================================================================

typedef uint32_t txColor;

//{===========================================================================================================
/**
 * @ingroup Miscellanous
 * @brief Тип данных для хранения размеров объекта(ширина и высота)
 *
 * @code
 * ...
 * txSize size = {
 *     .sizeX = 800,
 *     .sizeY = 640
 * };
 * ...
 * @endcode
 *
 * @see txPoint
 */
//}===========================================================================================================

struct txSize {
	long unsigned sizeX; /** < Ширина объекта */
	long unsigned sizeY; /** < Высота объекта */
};

//{===========================================================================================================
/**
 * @ingroup Miscellanous
 * @beief Тип данных для хранения координат объектов(напр. вершин многоугольников)
 *
 * @code
 * ...
 * txPoint coords[] = {
 *     { .coordX = 0, .coordY = 0 },
 *     ...
 * };
 * ...
 * @endcode
 *
 * @see txSize, txPoint2XPoint(), XPoint2txPoint()
 */
//}===========================================================================================================

struct txPoint {
	long signed coordX;
	long signed coordY;
};

namespace __TXLibX_SYS__ {
	namespace window {
		Display *display = 0;
		Window wnd       = 0;
		int screen       = 0;
		pthread_t thread = 0;
		bool running     = false;

		txPoint pos      = ZERO(txPoint);
		txSize size      = ZERO(txSize);
	}

	namespace user {
		int lock = 0;
	}

	namespace kb {
		int keys[256] = {};
	}

	namespace mouse {
		txPoint pos = { .coordX = 0, .coordY = 0 };
		uint32_t buttons = 0;
	}

	namespace BackBuf {
		Visual *vis = 0;
		XdbeBackBuffer back_buf = 0;
		GC context = 0;
		pthread_mutex_t lock = {};
	}

	namespace internal {
		const char *ver = "TXLib v1.5 (X Window System branch)";
	}

	namespace logger {
		const char *func;
		const char *file;
		int line;

		class tx_logger {
			FILE *log;
		public:
			tx_logger():
				log(0)
			{ }

			tx_logger(const char * const file, int line, 
			          const char * const func):
				log(fopen("txlog.log", "a+"))
			{
 				assert(log);
				fprintf(log, "Entring into %s function "
				             "in %s file on %d line!\n", 
				             TX_FUNC, TX_FILE, TX_LINE);
			}

			~tx_logger()
			{
				if (log) {
					fprintf(log, "Exiting from %s function "
					             "in %s file on %d line!\n",
					             TX_FUNC, TX_FILE, TX_LINE);
					fclose(log);
				}
			}
		};
	}
}

//{===========================================================================================================
/**
 * @ingroup Miscellanous
 * @brief Функция для блокировки параллельного потока
 *
 * @arg wait срочно ли нужно заблокировать другой поток программы
 *
 * TXLibX запускает параллельно 2 потока(почти как 2 программы):
 * в одном работает окно, а в другом - пользователь. 
 *
 * Оба этих потока используют одни и те же данные: например изоражение.
 * И может произойти так, что одновременно вспомогательный поток 
 * перерисовывет на окно изображение и пользователь, одновременно с этим,
 * каким-либо образом меняет изображение.
 *
 * Для того, чтобы избежать подобного рода проблем в TXLibX есть функция txLock().
 * Она блокирует обновление окна/рисование в окно на короткий промежуток времени,
 * А потом - txUnlock() разблокирует это операцию.
 *
 * @code
 * ...
 * txLock(false); // force lock
 * rendering();
 * txUnlock();
 * ...
 * @endcode
 * 
 * @return успешна ли закончилась блокировка
 *
 * @see txUnlock()
 */
//}===========================================================================================================

inline int txLock(bool wait = true);

//{===========================================================================================================
/**
 * @ingroup Miscellanous
 * @brief Функция для разблокировки параллельного потока
 * 
 * TXLibX запускает параллельно 2 потока(почти как 2 программы):
 * в одном работает окно, а в другом - пользователь. 
 *
 * Оба этих потока используют одни и те же данные: например изоражение.
 * И может произойти так, что одновременно вспомогательный поток 
 * перерисовывет на окно изображение и пользователь, одновременно с этим,
 * каким-либо образом меняет изображение.
 *
 * Для того, чтобы избежать подобного рода проблем в TXLibX есть функция txLock().
 * Она блокирует поток, выполняющий обновление окна/рисование в окно и т.д... 
 * на короткий промежуток времени, а потом - txUnlock() разблокирует эту операцию.
 *
 * @code
 * ...
 * txLock(false); // force lock
 * rendering();
 * txUnlock();
 * ...
 * @endcode
 *
 * @return успешно ли закончилась разблокировка
 *
 * @see txLock()
 */
//}===========================================================================================================

inline int txUnlock();

//{===========================================================================================================
/**
 * @ingroup Miscellanous
 * @brief Функция для разблокировки параллельного потока
 * 
 * @tparam T тип возвращаемого значения
 *
 * @arg val возвращаемое значение
 *
 * TXLibX запускает параллельно 2 потока(почти как 2 программы):
 * в одном работает окно, а в другом - пользователь. 
 *
 * Оба этих потока используют одни и те же данные: например изоражение.
 * И может произойти так, что одновременно вспомогательный поток 
 * перерисовывет на окно изображение и пользователь, одновременно с этим,
 * каким-либо образом меняет изображение.
 *
 * Для того, чтобы избежать подобного рода проблем в TXLibX есть функция txLock().
 * Она блокирует поток, выполняющий обновление окна/рисование в окно и т.д... 
 * на короткий промежуток времени, а потом - txUnlock() разблокирует эту операцию.
 *
 * @code
 * ...
 * txLock(false); // force lock
 * rendering();
 * txUnlock();
 * ...
 * @endcode
 *
 * @return val
 *
 * @see txLock()
 */
//}===========================================================================================================

template<typename T>
inline T txUnlock(T val);

//{===========================================================================================================
/**
 * @ingroup Drawing
 * @brief Функция для проверки готовности окна к рисованию
 *
 * Данная функция проверяет окно на готовночть к рисованию.
 * К окну подключен контекст рисования и framebuffer.
 * Также окно подключено к серверу X11.
 * txOK() проверяет готовность окна по всем этим критериям:
 * <ol>
 *   <li>Подключилась ли программа к серверу X11</li>
 *   <li>Создано ли уже окно</li>
 *   <li>Создан ли framebuffer для окна</li>
 *   <li>Подключено ли окно к этому framebuffer'у</li>
 *   <li>Закончила ли свою работу функция txCreateWindow()</li>
 * </ol>
 *
 * @code
 * void rendering() 
 * {
 *     if (!txOK()) {
 *         fprintf(stderr, "Fatal fail!\nTXLib isn't already running when calling rendereing routine!\n");
 *         exit(EXIT_FAILURE);
 *     }
 *     ...
 * }
 * @endcode
 *
 * @return готово ли окно к рисованию
 *
 * @see txBackBuf(), txDisplay(), txScreen(), txWindow(), txGC()
 */
//}===========================================================================================================

inline bool txOK();

//{===========================================================================================================
/**
 * @ingroup Drawing
 * @brief Функция для получения буфера TXLib
 *
 * Окно TXLibX имеет кадровый буфер, для хранения изображения. И время от времени
 * содержимое этого буфера перерисовывется на окно.
 * Иногда, рисование занимает долгое время:
 * например - программный рендеринг сферы большого размера.
 * И, зачастую людям приятнее видеть 'Loading...', чем мелькающие кусочки неполноценного
 * изображения.
 * Перерисовка из буфера может быть заблокирована на время, но чтобы хранить изменения в изображении
 * нужен этот самый буфер.
 *
 * @code
 * ...
 * XDrawArcs(txDisplay(), txBackBuf(), txGC(), arcs, sizeof(arcs) / sizeof(XArc));
 * ...
 * @endcode
 *
 * @return промежуточный буфер TXLib
 *
 * @see txOK(), txDisplay(), txScreen, txWindow, txGC()
 */
//}===========================================================================================================

inline XdbeBackBuffer txBackBuf();

//{===========================================================================================================
/**
 * @ingroup Drawing
 * @brief Функция для получения подключения к X11 Server.
 *
 * Данная функция возвращает подключение TXLib'а к X Window Server
 * Но перед этим, она проверяет, готово ли окно TXLib к рисованию или нет.
 * Ну мало ли, набудокурят еще от славного кошачего имени TXLib'а...
 *
 * @code
 * ...
 * XDrawArcs(txDisplay(), txBackBuf(), txGC(), arcs, sizeof(arcs) / sizeof(XArc));
 * ...
 * @endcode
 *
 * @see txOK(), txBackBuf(), txScreen(), txWindow(), txGC()
 */
//}===========================================================================================================

inline Display *txDisplay();

//{===========================================================================================================
/**
 * @ingroup Drawing
 * @brief Функция для получения дефолтного экрана для txDisplay()
 *
 * Данная функция воозвращает дефолтный экран для текущего подключения TXLib'а к
 * X Window Server.
 * 
 * @code
 * printf("──────▄█████████▄──────────────\n"
 *        "────▄██▀─▀████████▄────────────\n"
 *        "───██──▀██▄█████████───────────\n"
 *        "───██────▀██▀─▀███▀────────────\n"
 *        "────██───▄█────────────────────\n"
 *        "─────███──██───────────────────\n"
 *        "───────██──██──────────────────\n"
 *        "────────██──███────────────────\n"
 *        "────────█─██──██───────────────\n"
 *        "────────██─██──██──────██──────\n"
 *        "────────██─███──███──█████─────\n"
 *        "───────████████──█████████─────\N
 *        "─────██─██▀▀▀▀▀▀▀▀▀▀▀▀▀████────\n"
 *        "───███─█▀─────────────────███──\n"
 *        "──███─█▀─────────────▄███▄──██─\n"
 *        "███──█▀────────────▄███████──██\n"
 *        "██──█▀──▄█▄──▐─▌──██▀─█──██─█─█\n"
 *        "█───█────▀──▄▀▀▀▄─▀█▄──▄██──█─█\n"
 *        "█───█────────███─▌──▀███▀──██─█\n"
 *        "██──█▄────────▌──▐────────██──█\n
 *        "██───█▄─────▐▄▌──▌──────▄█───██\n"
 *        "███───██▄────▌──▐────▄██▀───██─\n"
 *        "─██────▀███▄─▀▀▀▀─▄███▀─────██─\n"
 *        "──██──────▀████████▀───────██──\n"
 *        "───██────────▄██▄─────────██───\n"
 *        "───███──────▀████▀───────██────\n"
 *        "────███▄▄─────▀▀─────▄▄███─────\n"
 *        "────██████████████████████─────\n"
 *        "───███──██──────────██──███────\n"
 *        "────────██──────────██─────────\n"
 *        "────────███────────███─────────\n");
 *
 * printf("\nOh yes, you wanted screenID: %d\n", txScreen());
 * @endcode
 *
 * @return ID дефолтного экрана TXLib'а для подключения последнего к X Window Server
 *
 * @see txOK(), txBackBuf(), txDisplay(), txWindow(), txGC()
 */
//}===========================================================================================================

inline int txScreen();

//{===========================================================================================================
/**
 * @ingroup Drawing
 * @brief Функция для получения дескриптора окна TXLib'а
 *
 * Данная функция возвращает дескриптор окна TXLib'а.
 * Кстати вы, если бы хотели, могли ы рисовать прямо на нем, минуя txBackBuf().
 *
 * @code
 * GC gc = XCreateGC(tx:)
 * @endcode
 */
//}===========================================================================================================

inline Window txWindow();
inline GC txGC();
inline const char *txVersion();

Window txCreateWindow(unsigned sizeX, unsigned sizeY);
inline constexpr txSize txDefaultWindowSize();

inline txSize txWindowSize();
inline long unsigned txWindowWidth();
inline long unsigned txWindowHeight();
inline txPoint txWindowPos();
inline long signed txWindowX();
inline long signed txWindowY();
inline txSize txScreenSize();
inline long unsigned txScreenWidth();
inline long unsigned txScreenHeight();

inline constexpr txColor txRGB(uint8_t red, uint8_t green, uint8_t blue);

inline void txSetColor(txColor clr);
inline txColor txGetColor();

inline void txBegin();
inline void txEnd();

inline constexpr XPoint txPoint2XPoint(txPoint p0);
inline constexpr txPoint XPoint2txPoint(XPoint p0);

inline void txSetPixel(txPoint p0);
inline void txSetPixel(long signed p0x, long signed p0y);

inline void txLine(txPoint p0, txPoint p1);
inline void txLine(long signed p0x, long signed p0y,
                   long signed p1x, long signed p1y);

inline void txRectangle(txPoint p0, txPoint p1);
inline void txRectangle(long signed p0x, long signed p0y,
                        long signed p1x, long signed p1y);

inline void txArc(txPoint p0, txPoint p1, 
                  double begin, double end);
inline void txArc(long signed p0x, long signed p0y,
                  long signed p1x, long signed p1y,
                  double begin, double end);

inline void txPie(txPoint p0, txPoint p1,
                  double begin, double end);
inline void txPie(long signed p0x, long signed p0y,
                  long signed p1x, long signed p1y,
                  double begin, double end);

inline void txEllipse(txPoint p0, txPoint p1);
inline void txEllipse(long signed p0x, long signed p0y,
                      long signed p1x, long signed p1y);

inline void txCircle(txPoint p0, long unsigned rad);
inline void txCircle(long signed p0x, long signed p0y,
                     long unsigned rad);

inline void txPolygon(txPoint points[], int points_sz);
inline void txPolygon(XPoint points[], int points_sz);

inline void txClear();

inline int txGetKeyState(long unsigned id);
inline txPoint txMousePos();
inline long signed txMouseX();
inline long signed txMouseY();
inline long unsigned txMouseButtons();

inline void txSleep(long ms);

void txSysCommand(std::string cmd, std::string &res);
::std::string txGetBundlePath();

#ifdef __DOXYGEN__
enum txColors: txColor {
#else
const txColor
#endif
	TX_BLACK         = txRGB(  0,   0,   0),   
	TX_BLUE          = txRGB(  0,   0, 128),   
	TX_GREEN         = txRGB(  0, 128,   0),   
	TX_CYAN          = txRGB(  0, 128, 128),   
	TX_RED           = txRGB(128,   0,   0),   
	TX_MAGENTA       = txRGB(128,   0, 128),   
	TX_BROWN         = txRGB(128, 128,   0),   
	TX_ORANGE        = txRGB(255, 128,   0),   
	TX_GRAY          = txRGB(160, 160, 160),   
	TX_DARKGRAY      = txRGB(128, 128, 128),   
	TX_LIGHTGRAY     = txRGB(192, 192, 192),   
	TX_LIGHTBLUE     = txRGB(  0,   0, 255),   
	TX_LIGHTGREEN    = txRGB(  0, 255, 128),   
	TX_LIGHTCYAN     = txRGB(  0, 255, 255),   
	TX_LIGHTRED      = txRGB(255,   0, 128),   
	TX_LIGHTMAGENTA  = txRGB(255,   0, 255),   
	TX_PINK          = txRGB(255, 128, 255),   
	TX_YELLOW        = txRGB(255, 255, 128),   
	TX_WHITE         = txRGB(255, 255, 255);
#ifdef __DOXYGEN__
};
#endif

void __txCanvas_RegisterClose__()
{
	$TX_ENTRY
	TX_FATAL_FAIL

	Atom delete_window = XInternAtom(::__TXLibX_SYS__::window::display,
	                                 "WM_DELETE_WINDOW", 0);
	XSetWMProtocols(::__TXLibX_SYS__::window::display, 
	                ::__TXLibX_SYS__::window::wnd,
	                &delete_window, 1);
}

void __txCanvas_OnExpose__()
{
	$TX_ENTRY
	TX_FATAL_FAIL

	if (!(::__TXLibX_SYS__::user::lock)) {
		txLock(false);

		XdbeBeginIdiom(txDisplay());

		XdbeSwapInfo info = {};
		info.swap_window = txWindow();
		info.swap_action = XdbeBackground;

		if (!XdbeSwapBuffers(txDisplay(), &info, 1))
			TX_ERROR("Can't swap back window buffer!")

		XdbeEndIdiom(txDisplay());

		txUnlock();
	}
}

bool __txCanvas_OnKB__(XKeyEvent ev)
{
	$TX_ENTRY
	TX_FATAL_FAIL

	if (ev.keycode == 0x9)
		return true;

	::__TXLibX_SYS__::kb::keys[ev.keycode] = (ev.type == KeyPress);

	return false;
}

void __txCanvas_OnMouse__(XEvent ev)
{
	$TX_ENTRY
	TX_FATAL_FAIL

	switch (ev.type) {
	case MotionNotify:
		::__TXLibX_SYS__::mouse::pos = txPoint{ev.xmotion.x_root, ev.xmotion.y_root};
		::__TXLibX_SYS__::mouse::buttons &= ((ev.xmotion.state >> 8) & 0b11111);
		break;

	case ButtonPress: {
		::__TXLibX_SYS__::mouse::pos = {ev.xbutton.x_root, ev.xbutton.y};
		::__TXLibX_SYS__::mouse::buttons |= ((ev.xbutton.state >> 8) & 0b11111);
		break;
	}

	case ButtonRelease: {
		::__TXLibX_SYS__::mouse::pos = {ev.xbutton.x_root, ev.xbutton.y_root};
		::__TXLibX_SYS__::mouse::buttons &= ~((ev.xbutton.state >> 8) & 0b11111);
		break;
	}
	}
}

void __txCanvas_WindowLoop__()
{
	$TX_ENTRY
	TX_FATAL_FAIL

	__txCanvas_RegisterClose__();

	XEvent ev = {};

#define STOP() goto __exit__;
	while (1) {
		XNextEvent(txDisplay(), &ev);

		switch (ev.type) {
		case Expose:
			__txCanvas_OnExpose__();
			break;

		case KeyPress: 
		case KeyRelease:
			if (__txCanvas_OnKB__(ev.xkey))
				STOP();
			break;

		case ButtonPress:
		case ButtonRelease:
		case MotionNotify:
			__txCanvas_OnMouse__(ev);
			break;

		case ClientMessage:
			STOP();
		}
	}
#undef STOP

__exit__:
	::__TXLibX_SYS__::window::running = false;
}

void __txCanvas_SetupXdbe__()
{
	$TX_ENTRY
	TX_FATAL_NONFAIL

	assert(::__TXLibX_SYS__::window::display);

	int major = 0, minor = 0;
	if (XdbeQueryExtension(::__TXLibX_SYS__::window::display, 
	                       &major, &minor)) {
		int num_scr = 0;
		Drawable screens[] = {DefaultRootWindow(::__TXLibX_SYS__::window::display)};
		XdbeScreenVisualInfo *info = XdbeGetVisualInfo(
			::__TXLibX_SYS__::window::display,
			screens,
			&num_scr);

		if (!info || num_scr < 1 || info->count < 1)
			TX_ERROR("Error!\nFailed to query XDBE %d.%d!\n", major, minor)

		XVisualInfo visinfo_template = {};
		visinfo_template.visualid = info->visinfo->visual;
		visinfo_template.screen = 0;
		visinfo_template.depth = info->visinfo->depth;

		int match = 0;
		XVisualInfo *vis = XGetVisualInfo(::__TXLibX_SYS__::window::display,
		                                  VisualIDMask | VisualScreenMask | VisualDepthMask,
		                                  &visinfo_template, &match);

		if (!vis || match < 1)
			TX_ERROR("Error!\nFailed to query visual for XDBE!\n")

		::__TXLibX_SYS__::BackBuf::vis = vis->visual;
	} else
		TX_ERROR("Error!\nXDBE is not supported on the machine")
}

void __txCanvas_BindXdbe__()
{
	$TX_ENTRY
	TX_FATAL_NONFAIL

	assert(::__TXLibX_SYS__::window::display);
	assert(::__TXLibX_SYS__::window::wnd);
	assert(::__TXLibX_SYS__::BackBuf::vis);

	::__TXLibX_SYS__::BackBuf::back_buf = XdbeAllocateBackBufferName(
		::__TXLibX_SYS__::window::display, ::__TXLibX_SYS__::window::wnd,
		XdbeBackground);

	::__TXLibX_SYS__::BackBuf::context = XCreateGC(
		::__TXLibX_SYS__::window::display,
		::__TXLibX_SYS__::BackBuf::back_buf,
		0, 0);

	XSetForeground(::__TXLibX_SYS__::window::display,
	               ::__TXLibX_SYS__::BackBuf::context,
	               WhitePixel(::__TXLibX_SYS__::window::display,
	                          ::__TXLibX_SYS__::window::screen));
}

void __txCanvas_SetupX11__()
{
	$TX_ENTRY
	TX_FATAL_NONFAIL

	::__TXLibX_SYS__::window::display = XOpenDisplay(0);
	::__TXLibX_SYS__::window::screen = DefaultScreen(::__TXLibX_SYS__::window::display);
}

void __txCanvas_TryInitLockMutex__()
{
	$TX_ENTRY
	TX_FATAL_FAIL

	int val = pthread_mutex_init(&(::__TXLibX_SYS__::BackBuf::lock), 0);
	if (val)
		TX_ERROR("Failed to initialize mutex: %s", strerror(val))
}

void __txCanvas_CleanUP__()
{
	$TX_ENTRY
	TX_FATAL_NONFAIL

	if (::__TXLibX_SYS__::window::thread) {
		pthread_join(::__TXLibX_SYS__::window::thread, 0);
		::__TXLibX_SYS__::window::thread = 0;
		pthread_mutex_destroy(&(::__TXLibX_SYS__::BackBuf::lock));
		::__TXLibX_SYS__::BackBuf::lock = {};
	}

	if (::__TXLibX_SYS__::window::display) {
		XdbeDeallocateBackBufferName(
			::__TXLibX_SYS__::window::display,
		        ::__TXLibX_SYS__::BackBuf::back_buf);
		XCloseDisplay(::__TXLibX_SYS__::window::display);

		::__TXLibX_SYS__::window::display = 0;
		::__TXLibX_SYS__::window::wnd = 0;
		::__TXLibX_SYS__::window::screen = 0;
		::__TXLibX_SYS__::window::pos = {};
		::__TXLibX_SYS__::window::size = {};
		::__TXLibX_SYS__::user::lock = 0;
		::__TXLibX_SYS__::BackBuf::vis = 0;
		::__TXLibX_SYS__::BackBuf::back_buf = 0;
	}
}

void __txCanvas_OnQuit__()
{
	$TX_ENTRY

	static char *buf = reinterpret_cast<char *>(malloc(TX_BIG_BUFSIZE));
	if (!buf) {
		TX_ERROR("Can't allocate memory for buffer: %s",
		         strerror(errno));
	}

	if (::__TXLibX_SYS__::window::display && ::__TXLibX_SYS__::window::wnd) {
		XFetchName(::__TXLibX_SYS__::window::display, 
		           ::__TXLibX_SYS__::window::wnd, &buf);
		sprintf(buf, "%s - [ENDED]", buf);

		XStoreName(::__TXLibX_SYS__::window::display, 
		           ::__TXLibX_SYS__::window::wnd, buf);
	}

	while (::__TXLibX_SYS__::window::running)
		txSleep(0);

	__txCanvas_CleanUP__();
}

void __txInternalSigHandler__(int errid)
{
	static const ::std::map<int, const char *> sigmsg = {
		{SIGABRT, "Abnormal termination, alike abort()'s call gives"},
		{SIGFPE,  "Erroneous arithmetic operation. Maybe FPU divide by zero, or overflow"},
		{SIGILL,  "Illegal instruction found"},
		{SIGINT,  "Termination requested by user"},
		{SIGSEGV, "Segmentation violation: not permitted to use some data"},
		{SIGTERM, "Termination requested by application"}
	};

	if (sigmsg.find(errid) == sigmsg.end())
		TX_ERROR("Unknown error (%d)", errid);

	TX_ERROR("Runtime error: %s", sigmsg.at(errid));
}

int __txInternalAttachSigHandler__()
{
	signal(SIGABRT, __txInternalSigHandler__);
	signal(SIGFPE,  __txInternalSigHandler__);
	signal(SIGILL,  __txInternalSigHandler__);
	signal(SIGINT,  __txInternalSigHandler__);
	signal(SIGSEGV, __txInternalSigHandler__);
	signal(SIGTERM, __txInternalSigHandler__);

	return 0;
}

namespace __TXLibX_SYS__{
	namespace local {
		auto __tx_local_only_quit__ = atexit(__txCanvas_OnQuit__);
		auto __tx_local_only_sig__ = __txInternalAttachSigHandler__();
	}
}

void *__txCanvas_CreateWindow__(void *sz)
{
	$TX_ENTRY
	TX_FATAL_NONFAIL

	txSize window_size = {};
	XSizeHints hints = {};
	XSetWindowAttributes attrs = {};

	if (!sz)
		window_size = txDefaultWindowSize();
	else
		window_size = *(txSize *)sz;

	::__TXLibX_SYS__::window::size = window_size;

	hints.flags = PMinSize | PMaxSize;
	hints.max_width = hints.min_width = window_size.sizeX;
	hints.max_height = hints.min_height = window_size.sizeY;

	__txCanvas_SetupX11__();
	__txCanvas_SetupXdbe__();

	attrs.background_pixel = BlackPixel(::__TXLibX_SYS__::window::display,
	                                    ::__TXLibX_SYS__::window::screen);

	::__TXLibX_SYS__::window::wnd = XCreateWindow(
		::__TXLibX_SYS__::window::display,
	        DefaultRootWindow(::__TXLibX_SYS__::window::display),
		0, 0, window_size.sizeX, window_size.sizeY, 0,
		CopyFromParent, InputOutput, ::__TXLibX_SYS__::BackBuf::vis,
		CWBackPixel, &attrs);

	::std::string caption = txGetBundlePath() + " - TXLib";
	XStoreName(::__TXLibX_SYS__::window::display,
	           ::__TXLibX_SYS__::window::wnd,
	           caption.c_str());

	__txCanvas_BindXdbe__();

	XSetWMNormalHints(::__TXLibX_SYS__::window::display,
	                  ::__TXLibX_SYS__::window::wnd,
	                  &hints);
	XSelectInput(::__TXLibX_SYS__::window::display,
	             ::__TXLibX_SYS__::window::wnd,
	             KeyPressMask | KeyReleaseMask |
	             ButtonPressMask | ButtonReleaseMask |
	             PointerMotionMask | ExposureMask);

	XMapWindow(::__TXLibX_SYS__::window::display,
	           ::__TXLibX_SYS__::window::wnd);

	::__TXLibX_SYS__::window::running = true;

	__txCanvas_TryInitLockMutex__();
	__txCanvas_WindowLoop__();

	return 0;
}

inline int txLock(bool wait /*= true*/)
{
	$TX_ENTRY

	return ((wait)?
	        (pthread_mutex_lock(&__TXLibX_SYS__::BackBuf::lock)):
                (pthread_mutex_trylock(&__TXLibX_SYS__::BackBuf::lock)));
}

inline int txUnlock()
{
	$TX_ENTRY

	return pthread_mutex_unlock(&__TXLibX_SYS__::BackBuf::lock);
}

template<typename T>
inline T txUnlock(T val)
{
	$TX_ENTRY

	txUnlock();

	return val;
}

inline bool txOK()
{
	$TX_ENTRY

	return (::__TXLibX_SYS__::window::display &&
	        ::__TXLibX_SYS__::window::wnd &&
	        ::__TXLibX_SYS__::window::running);
}

inline XdbeBackBuffer txBackBuf()
{
	$TX_ENTRY
	TX_FATAL_FAIL

	return ::__TXLibX_SYS__::BackBuf::back_buf;
}

inline Display *txDisplay()
{
	$TX_ENTRY
	TX_FATAL_FAIL

	return ::__TXLibX_SYS__::window::display;
}

inline int txScreen()
{
	$TX_ENTRY
	TX_FATAL_FAIL

	return ::__TXLibX_SYS__::window::screen;
}

inline Window txWindow()
{
	$TX_ENTRY
	TX_FATAL_FAIL

	return ::__TXLibX_SYS__::window::wnd;
}

inline GC txGC()
{
	$TX_ENTRY
	TX_FATAL_FAIL

	return ::__TXLibX_SYS__::BackBuf::context;
}

inline const char *txVersion()
{
	$TX_ENTRY

	return ::__TXLibX_SYS__::internal::ver;
}

Window txCreateWindow(unsigned sizeX, unsigned sizeY)
{
	$TX_ENTRY
	TX_FATAL_NONFAIL

	XInitThreads();

	int err = 0;
	txSize size = {};
	size.sizeX = sizeX;
	size.sizeY = sizeY;

	err = pthread_create(&(::__TXLibX_SYS__::window::thread), 0, __txCanvas_CreateWindow__, &size);
	if (err)
		TX_ERROR("TXLib can't start window thread: %s", strerror(err));

	clock_t begin = clock();
	while (!txOK() && (begin + TX_TIMEOUT > clock()))
		txSleep(0);

	return txWindow();
}

inline constexpr txSize txDefaultWindowSize()
{
	return txSize{ 
		.sizeX = 800, 
		.sizeY = 600
	};
}

inline txSize txWindowSize()
{
	$TX_ENTRY
	TX_FATAL_FAIL
	
	return ::__TXLibX_SYS__::window::size;
}

inline long unsigned txWindowWidth()
{
	$TX_ENTRY
	TX_FATAL_FAIL

	return txWindowSize().sizeX;
}

inline long unsigned txWindowHeight()
{
	$TX_ENTRY
	TX_FATAL_FAIL

	return txWindowSize().sizeY;
}

inline txPoint txWindowPos()
{
	$TX_ENTRY
	TX_FATAL_FAIL

	return ::__TXLibX_SYS__::window::pos;
}

inline long signed txWindowX()
{
	$TX_ENTRY
	TX_FATAL_FAIL

	return txWindowPos().coordX;
}

inline long signed txWindowY()
{
	$TX_ENTRY
	TX_FATAL_FAIL

	return txWindowPos().coordY;
}

inline txSize txScreenSize()
{
	$TX_ENTRY

	Display *dis = XOpenDisplay(0);

	txSize size = {
		.sizeX = static_cast<long unsigned>(DefaultScreenOfDisplay(dis)->width),
		.sizeY = static_cast<long unsigned>(DefaultScreenOfDisplay(dis)->height)
	};

	XCloseDisplay(dis);

	return size;
}

inline long unsigned txScreenWidth()
{
	$TX_ENTRY

	return txScreenSize().sizeX;
}

inline long unsigned txScreenHeight()
{
	$TX_ENTRY

	return txScreenSize().sizeY;
}

inline constexpr txColor txRGB(uint8_t red, uint8_t green, uint8_t blue)
{
	return ((red << 16) & 0x00FF0000) |
	       ((green << 8) & 0x0000FF00) |
	       ((blue) & 0x000000FF);
}


inline void txSetColor(txColor clr)
{
	$TX_ENTRY
	TX_FATAL_FAIL

	$txDraw(XSetForeground(txDisplay(), 
			       txGC(), clr));
}

inline txColor txGetColor()
{
	$TX_ENTRY
	TX_FATAL_FAIL

	XGCValues data = {};
	XGetGCValues(txDisplay(), txGC(), GCForeground, &data);

	return data.foreground;
}

inline void txBegin()
{
	$TX_ENTRY
	TX_FATAL_FAIL

	::__TXLibX_SYS__::user::lock++;
}

inline void txEnd()
{
	$TX_ENTRY
	TX_FATAL_FAIL

	::__TXLibX_SYS__::user::lock--;
}

inline constexpr XPoint txPoint2XPoint(txPoint p0)
{
	return XPoint{static_cast<short signed>(p0.coordX), static_cast<short signed>(p0.coordY)};
}

inline constexpr txPoint XPoint2txPoint(XPoint p0)
{
	return txPoint{static_cast<long signed>(p0.x), static_cast<long signed>(p0.y)};
}

inline void txSetPixel(txPoint p0)
{
	$TX_ENTRY
	TX_FATAL_FAIL

	txSetPixel(p0.coordX, p0.coordY);
}

inline void txSetPixel(long signed p0x, long signed p0y)
{
	$TX_ENTRY
	TX_FATAL_FAIL

	$txDraw(XDrawPoint(txDisplay(),
	                   txBackBuf(),
	                   txGC(), p0x, p0y));
}

inline void txLine(txPoint p0, txPoint p1)
{
	$TX_ENTRY
	TX_FATAL_FAIL

	txLine(p0.coordX, p0.coordY, p1.coordX, p1.coordY);
}

inline void txLine(long signed p0x, long signed p0y,
                   long signed p1x, long signed p1y)
{
	$TX_ENTRY
	TX_FATAL_FAIL

	$txDraw(XDrawLine(txDisplay(), 
	                  txBackBuf(),
	                  txGC(), p0x, p0y, p1x, p1y));
}

inline void txRectangle(txPoint p0, txPoint p1)
{
	$TX_ENTRY
	TX_FATAL_FAIL

	txRectangle(p0.coordX, p0.coordY, p1.coordX, p1.coordY);
}

inline void txRectangle(long signed p0x, long signed p0y,
                        long signed p1x, long signed p1y)
{
	$TX_ENTRY
	TX_FATAL_FAIL

	long signed maxX = ::std::max(p0x, p1x);
	long signed minX = ::std::min(p0x, p1x);
	long signed maxY = ::std::max(p0y, p1y);
	long signed minY = ::std::min(p0y, p1y);

	$txDraw(XFillRectangle(txDisplay(),
                               txBackBuf(),
	                       txGC(), minX, minY, 
	                       maxX - minY, maxY - minY));
}

inline void txArc(txPoint p0, txPoint p1,
                  double begin, double end)
{
	$TX_ENTRY
	TX_FATAL_FAIL

	txArc(p0.coordX, p0.coordY, p1.coordX, p1.coordY,
              begin, end);
}

inline void txArc(long signed p0x, long signed p0y,
                  long signed p1x, long signed p1y,
                  double begin, double end)
{
	$TX_ENTRY
	TX_FATAL_FAIL

	$txDraw(XDrawArc(txDisplay(),
                         txBackBuf(),
	                 txGC(), p0x, p0y, p1x - p0x, p1y - p0y,
	                 begin * 64, (end - begin) * 64));
}

inline void txPie(txPoint p0, txPoint p1,
                  double begin, double end)
{
	$TX_ENTRY
	TX_FATAL_FAIL

	txPie(p0.coordX, p0.coordY, p1.coordX, p1.coordY,
	      begin, end);
}

inline void txPie(long signed p0x, long signed p0y,
                  long signed p1x, long signed p1y,
                  double begin, double end)
{
	$TX_ENTRY
	TX_FATAL_FAIL

	$txDraw(XFillArc(txDisplay(),
	                 txBackBuf(),
	                 txGC(), p0x, p0y, p1x, p1y,
                         begin * 64, (end - begin) * 64));
}

inline void txEllipse(txPoint p0, txPoint p1)
{
	$TX_ENTRY
	TX_FATAL_FAIL

	txEllipse(p0.coordX, p0.coordY, p1.coordX, p1.coordY);
}

inline void txEllipse(long signed p0x, long signed p0y,
                      long signed p1x, long signed p1y)
{
	$TX_ENTRY
	TX_FATAL_FAIL

	if (p0x > p1x)
		::std::swap(p0x, p1x);

	if (p0y > p1y)
		::std::swap(p0y, p1y);

	txPie(p0x, p0y, p1x - p0x, p1y - p0y, 0, 360);
}

inline void txCircle(txPoint p0, long unsigned rad)
{
	$TX_ENTRY
	TX_FATAL_FAIL

	txCircle(p0.coordX, p0.coordY, rad);
}

inline void txCircle(long signed p0x, long signed p0y,
                     long unsigned rad)
{
	$TX_ENTRY
	TX_FATAL_FAIL

	txEllipse(p0x - rad, p0y - rad, p0x + rad, p0y + rad);
}

inline void txPolygon(txPoint points[], int points_sz)
{
	$TX_ENTRY
	TX_FATAL_FAIL

	assert(points);

	XPoint *vertices = reinterpret_cast<XPoint *>(calloc(points_sz, sizeof(XPoint)));
	for (int i = 0; i < points_sz; i++)
		vertices[i] = txPoint2XPoint(points[i]);

	txPolygon(vertices, points_sz);

	free(reinterpret_cast<void *>(vertices));
}

inline void txPolygon(XPoint points[], int points_sz)
{
	$TX_ENTRY
	TX_FATAL_FAIL

	assert(points);

	$txDraw(XFillPolygon(txDisplay(), txBackBuf(),
	                     txGC(), points, points_sz,
	                     Complex, CoordModeOrigin));
}

inline void txClear()
{
	$TX_ENTRY
	TX_FATAL_FAIL

	txRectangle(0, 0, txWindowWidth(), txWindowHeight());
}

inline int txGetKeyState(long unsigned id)
{
	$TX_ENTRY
	TX_FATAL_FAIL

	if (id > 256)
		TX_ERROR("No key #%lu", id)

	return ::__TXLibX_SYS__::kb::keys[id];
}

inline txPoint txMousePos()
{
	$TX_ENTRY
	TX_FATAL_FAIL

	return ::__TXLibX_SYS__::mouse::pos;
}

inline long signed txMouseX()
{
	$TX_ENTRY
	TX_FATAL_FAIL

	return txMousePos().coordX;
}

inline long signed txMouseY()
{
	$TX_ENTRY
	TX_FATAL_FAIL

	return txMousePos().coordY;
}

inline long unsigned txMouseButtons()
{
	$TX_ENTRY
	TX_FATAL_FAIL

	return ::__TXLibX_SYS__::mouse::buttons;
}

inline void txSleep(long ms)
{
	long timeout = ms / 1000 * CLOCKS_PER_SEC;
	clock_t begin = clock();

	while (timeout + begin >= clock());
}

void txSysCommand(::std::string cmd, ::std::string &res)
{
	$TX_ENTRY

	char buf[TX_BIG_BUFSIZE] = "";
	FILE *file = popen(cmd.c_str(), "r");

	if (!file) {
		TX_ERROR("Error!\nCan't exec '%s': %s!", 
		         cmd.c_str(), strerror(errno));
	}

	if (fgets(buf, TX_BIG_BUFSIZE - 1, file)) {
		res = buf;
	} else {
		TX_ERROR("Failed to execute: %s",
		         cmd.c_str());
	}

	pclose(file);	
}

::std::string txGetBundlePath()
{
	$TX_ENTRY

	pid_t proc = getpid();
	::std::string res = "";

#if !defined(__MACH__) && !defined(__APPLE__) && !defined(macintosh) && !defined(Macintosh)
	::std::stringstream cmd;

	cmd << "readlink /proc/" << proc << "/exe | sed "
	    << "\"s/\\(\\/" << "\\)$//\"";

	txSysCommand(cmd.str(), res);
#else
	char buf[PATH_MAX] = "";
	proc_pidpath(proc, buf, PATH_MAX);

	res = buf;
#endif

	return res;
}

#endif /* TXLIBX_H */
